#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to delete everything
"""

import sys
import time

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities 

driver = webdriver.Remote(
         command_executor='http://selenium-standalone-firefox:4444/wd/hub',
         desired_capabilities=DesiredCapabilities.FIREFOX)

# driver = webdriver.Firefox(executable_path="/home/jenny/Documents/python_scripts.git/geckodriver")

archivist_url = "https://closer-temp.herokuapp.com"
export_url = "https://closer-temp.herokuapp.com/admin/instruments"

def archivist_login(url, uname, pw):
    """
    Log in to archivist
    """
    driver.get(url)
    time.sleep(5)
    driver.find_element_by_id("login-email").send_keys(uname)
    driver.find_element_by_id("login-password").send_keys(pw)
    driver.find_element_by_class_name("btn-default").click()
    time.sleep(5)


def archivist_logout(driver, sleep_time=3):
    """
    Log out from archivist
    """
    
    try:
        driver.find_element_by_xpath('//a[@data-ng-click="sign_out()"]').click()
    except NoSuchElementException as e:
        print(e)
        return False
    time.sleep(sleep_time)
    # check next page's title to make sure one is logged out
    if not driver.title == "Home | Archivist":
        print("failed to log out")
        return False
    return True


def find_prefix_names(uname, pw):
    """
    Find all prefix names 
    """
    # log in
    archivist_login(archivist_url, uname, pw)
    driver.get(export_url)
    time.sleep(5)

    # find prefix names
    trs = driver.find_elements_by_xpath("html/body/div/div/div/div/div/div/table/tbody/tr")
    prefix_list = []
    for i in range(1, len(trs)):                
        # row 0 is header: tr has "th" instead of "td"
        tr = trs[i]

        # column 2 is "Prefix"
        prefix_name = tr.find_elements_by_xpath("td")[1].text
        prefix_list.append(prefix_name)

    # log out
    archivist_logout(driver)

    return(prefix_list)


def click_delete_button(uname, pw, prefix):
    """
    Click 'Delete' for the study
    """
    # log in
    archivist_login(archivist_url, uname, pw)
    driver.get(export_url)
    time.sleep(5)

    inputElement = driver.find_element_by_xpath('//input[@placeholder="Search for..."]')
    inputElement.send_keys(prefix)   

    # find prefix names
    trs = driver.find_elements_by_xpath("html/body/div/div/div/div/div/div/table/tbody/tr")
    tr = trs[1]

    # column 6 is "Actions", click on "Delete"
    deleteButton = tr.find_elements_by_xpath("td")[3].find_elements_by_tag_name('button')[-1] 
    print("Click delete button for " + prefix)
    deleteButton.click()

    time.sleep(5)
    driver.find_element_by_id("confirm-prefix").send_keys(prefix)
    driver.find_element_by_class_name("btn-danger").click()

    archivist_logout(driver)


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    prefix_list = find_prefix_names(uname, pw)

    for prefix in prefix_list:
        click_delete_button(uname, pw, prefix)


if __name__ == "__main__":
    main()
